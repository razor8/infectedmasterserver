#ifndef BYTES_H
#define BYTES_H

#include <stdint.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

template<typename T> static inline T switchByteOrder(T t){
	T ret;
	char *p = ((char*)&t);
	char *r = ((char*)&ret);
	for(int i = 0;i < sizeof(T);++i){
		r[i] = p[sizeof(T)-i];
	}
	//std::reverse_copy((char*)&t,(char*)&t+sizeof(T),(char*)&tmp);
	return ret;
}

template<typename T> static inline void switchByteOrderMutable(T &t){
	char tmp;
	char *p = ((char*)&t);
	for(int i = 0;i < sizeof(T);++i){
		tmp = p[i];
		p[i] = p[sizeof(T)-i];
		p[sizeof(T)-i] = tmp;
	}
}

namespace helix {
	using namespace std;

	//Bytes <<<<<<<<<<<<<<<<<<<<<<<<<<<<< 
	typedef basic_string<uint8_t> basic_bytes;
	typedef basic_bytes::size_type size_type;

	class bytes : public basic_bytes {
	public:
		bytes(void) : basic_bytes() {}
		explicit bytes(basic_bytes::size_type l, uint8_t c) : basic_bytes(l, c) {}
		virtual ~bytes(void) {}

		template<typename T> bytes(const T& b) : basic_bytes(convert_to_basic_bytes(b)) {}
		template<typename T> bytes(const T& b, const basic_bytes::size_type& l) : basic_bytes(convert_to_basic_bytes(b, l)) {}

		template<typename T> T as(void) const;
		bool compare(const bytes& b);
	private:
		template<typename T> basic_bytes convert_to_basic_bytes(const T& b);
		basic_bytes convert_to_basic_bytes(const string& b, const basic_bytes::size_type& l);
		basic_bytes convert_to_basic_bytes(const bytes& b, const basic_bytes::size_type& l);
		basic_bytes convert_to_basic_bytes(const void* b, const basic_bytes::size_type& l);
	};

	template<typename T> inline basic_bytes bytes::convert_to_basic_bytes(const T& b) {
		return basic_bytes(reinterpret_cast<const uint8_t*>(&b), sizeof(T));
	}

	template<> inline basic_bytes bytes::convert_to_basic_bytes<bytes>(const bytes& b) {
		return b;
	}

	template<> inline basic_bytes bytes::convert_to_basic_bytes<basic_bytes>(const basic_bytes& b) {
		return b;
	}

	template<> inline basic_bytes bytes::convert_to_basic_bytes<string>(const string& b) {
		return basic_bytes(reinterpret_cast<const uint8_t*>(b.data()), b.size());
	}

	inline basic_bytes bytes::convert_to_basic_bytes(const string& b, const size_type& l) {
		return basic_bytes(reinterpret_cast<const uint8_t*>(b.data()), l);
	}

	inline basic_bytes bytes::convert_to_basic_bytes(const bytes& b, const size_type& l) {
		return b.substr(0, l);
	}

	inline basic_bytes bytes::convert_to_basic_bytes(const void* b, const size_type& l) {
		return basic_bytes(reinterpret_cast<const uint8_t*>(b), l);
	}

	template<typename T> inline T bytes::as(void) const {
		if (this->size() != sizeof(T)) return T();
			//throw "bad size !";

		return *reinterpret_cast<const T*>(this->data());
	}

	template<> inline string bytes::as<string>(void) const {
		return string(reinterpret_cast<const char*>(this->data()), this->size());
	}

	template<> inline bytes bytes::as<bytes>(void) const {
		return *this;
	}

	bool inline bytes::compare(const bytes& b){
		if(this->size() != b.size()) return false;
		return memcmp(this->data(),b.data(),this->size()) == 0;
		return true;
	}

	//bytestream <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	class bytestream{
	public:
		bytestream(bytes& b) : m_bytes(b), m_offset(0),switch_byte_order(false) {}
		virtual ~bytestream(void) {}

		template<typename T> void put(const T& b);
		template<typename T> void put(const T& b, const size_type& l);
		template<typename T> void put_front(const T& b);
		template<typename T> void put_front(const T& b, const size_type& l);

		template<typename T> T get(void);
		template<typename T> T get(const size_type& l);

		template<typename T> void insert(const size_type& pos,const T& b);

		void clear(void);
		size_type get_offset(void);
		void set_offset(const size_type& pos);
		void erase(const size_type& length);
		void erase(const size_type& pos, const size_type& length);

		//void setSwitchByteOrder(bool value);

		size_type available(void);

		bool good();
	private:
		bytes& m_bytes;
		size_type m_offset;
		bool switch_byte_order;
	};

	//void bytestream::setSwitchByteOrder(bool value){
	//	this->switch_byte_order = value;
	//}

	inline size_type bytestream::available(void) {
		return this->m_bytes.size() - this->m_offset;
	}

	inline bool bytestream::good(){
		return available() > 0;
	}

	inline void bytestream::set_offset(const size_type& offset) {
		if(this->m_bytes.size() > offset) this->m_offset = offset;
	}

	inline size_type bytestream::get_offset(void) {
		return this->m_offset;
	}

	inline void bytestream::erase(const size_type& length){
		if(this->m_offset+length <= this->m_bytes.size())this->m_bytes.erase(this->m_offset,length);
	}

	inline void bytestream::erase(const size_type& pos,const size_type& length){
		if(pos+length <= this->m_bytes.size()){
			this->m_bytes.erase(pos,length);
			this->m_offset = pos;
		}
	}

	inline void bytestream::clear(void) {
		this->m_bytes = bytes();
		this->m_offset = 0;
	}

	template<typename T> void bytestream::insert(const size_type& pos,const T& b) {
		if(this->m_bytes.size() < pos) return;
		bytes tmp(b);
		this->m_bytes.insert(pos,tmp.data(),tmp.size());
		tmp.clear();
	}

	template<typename T> void bytestream::put(const T& b) {
		this->m_bytes += bytes(switch_byte_order ? switchByteOrder(b) : b);	
	}

	template<typename T> void bytestream::put(const T& b, const size_type& l) {
		this->m_bytes += bytes(b, l);
	}

	template<typename T> void bytestream::put_front(const T& b) {
		this->m_bytes = bytes(switch_byte_order ? switchByteOrder(b) : b) + this->m_bytes;
	}

	template<typename T> void bytestream::put_front(const T& b, const size_type& l) {
		this->m_bytes = bytes(b, l) + this->m_bytes;
	}

	template<typename T> inline T bytestream::get(void) {
		if (this->m_bytes.size() - this->m_offset < sizeof(T)) return T();
			//throw exception();

		T tmp = *reinterpret_cast<const T*>(this->m_bytes.data() + this->m_offset);
		if(switch_byte_order) switchByteOrderMutable(tmp);
		this->m_offset += sizeof(T);
		return tmp;
	}

	//use it only if you want to read a null-terminated string !
	//if it's not, use instead get<string>(length)
	template<> inline string bytestream::get<string>(void) {
		string tmp(reinterpret_cast<const char*>(this->m_offset + this->m_bytes.data()));
		this->m_offset += tmp.size() + 1;// +'\0'
		return tmp;
	}

	template<> inline string bytestream::get<string>(const size_type& l) {
		basic_bytes tmp = this->m_bytes.substr(this->m_offset, l);
		this->m_offset += tmp.size();
		return string(reinterpret_cast<const char*>(tmp.data()), tmp.size());
	}

	template<> inline bytes bytestream::get<bytes>(const size_type& l) {
		basic_bytes tmp = this->m_bytes.substr(this->m_offset, l);
		this->m_offset += tmp.size();
		return tmp;
	}

	//fbytestream <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	class fbytestream : public fstream{
	public:
		fbytestream(void) {}

		fbytestream(const std::string& fn, ios_base::openmode mode = ios_base::in | ios_base::out) : fstream(fn.c_str(), mode) {}

		void open(const std::string& fn, ios_base::openmode mode = ios_base::in | ios_base::out);

		template<typename T> T get(void);
		template<typename T> T get(const size_type& l);

		template<typename T> void put(const T& b);
		template<typename T> void put(const T& b, const size_type& l);

		size_type size(void);
	};

	inline void fbytestream::open(const std::string& fn, ios_base::openmode mode) {
		fstream::open(fn.c_str(), mode);
	}

	template<typename T> inline T fbytestream::get(void) {
		T ret = T();
		this->read(reinterpret_cast<char*>(&ret), sizeof(T));
		return ret;
	}

	template<> inline string fbytestream::get<string>(void) {
		string ret;

		char c = 0;
		while(this->read(&c, sizeof(char)) && c != 0)ret += c;

		return ret;
	}

	template<> inline string fbytestream::get<string>(const size_type& l) {
		char* tmp = new char[l];
		this->read(tmp, l);
		string ret(tmp, l);
		delete[] tmp;
		return ret;
	}

	template<> inline bytes fbytestream::get<bytes>(const size_type& l) {
		char* tmp = new char[l];
		this->read(tmp, l);
		bytes ret(tmp, l);
		delete[] tmp;
		return ret;
	}

	template<typename T> inline void fbytestream::put(const T& b) {
		this->write(reinterpret_cast<const char*>(&b), sizeof(T));
	}

	template<> inline void fbytestream::put<string>(const string& b) {
		this->write(b.data(), b.size());
	}

	template<> inline void fbytestream::put<bytes>(const bytes& b) {
		this->write(reinterpret_cast<const char*>(b.data()), b.size());
	}

	template<> inline void fbytestream::put<string>(const string& b, const size_type& l) {
		this->write(b.c_str(), l);
	}

	template<> inline void fbytestream::put<bytes>(const bytes& b, const size_type& l) {
		this->write(reinterpret_cast<const char*>(b.data()), l);
	}

	inline size_type fbytestream::size(void) {
		size_type p = this->tellg();
		this->seekg(0, std::fstream::end);
		size_type l = this->tellg();
		this->seekg(p);
		return l;
	}
}

#endif //BYTES_H
