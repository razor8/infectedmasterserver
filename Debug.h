#ifndef DEBUG_H
#define DEBUG_H

#if !defined(_DEBUG) || (!defined (_WIN32) && !defined(_WIN64))
	//RELEASE CONFIGURATION
#ifndef NDEBUG
	#define NDEBUG //suppress assert()
#endif

	#define HInitDebug()
	#define HDEBUGNAME(_name)
#else
	//DEBUG CONFIGURATION
#ifndef _CRTDBG_MAP_ALLOC
	#define _CRTDBG_MAP_ALLOC
#endif	
	#include <stdlib.h>
	#include <crtdbg.h>
		static void HInitDebug(void){		
			_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
			_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
		}
	#ifndef DBG_NEW
		#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ ) 
		#define new DBG_NEW 
	#endif

	//only use this when _DEBUG is defined!
	#define HDEBUGNAME(_name) const char * getDebugName(void){return _name;}

#endif //_DEBUG

#include <assert.h> //assert

#endif //DEBUG_H