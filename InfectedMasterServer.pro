#-------------------------------------------------
#
# Project created by QtCreator 2014-05-02T22:28:09
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = InfectedMasterServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    tcpserver.cpp \
    client.cpp

HEADERS += \
	Debug.h \
	Bytes.h \
    tcpserver.h \
    client.h \
    PacketDefinitions.h
