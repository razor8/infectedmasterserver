#ifndef PACKETDEFINITIONS_H
#define PACKETDEFINITIONS_H
 
#include <QHostAddress>
#include "Bytes.h"
#include "math.h"
#include "float.h"
#include "time.h"

const qint32 GAMESERVER_MAGIC = 0xAABBAABB;
const qint32 CLIENT_MAGIC = 0xBBAABBAA;
const qint32 GAMESERVERLIST_MAGIC = 0xABABABAB;
const qint32 CLIENTLIST_MAGIC = 0xBABABABA;
const qint32 MINIGAME_MAGIC = 0xABBAABBA;
const qint32 GAMEOVER_MAGIC = 0xBAABBAAB;

#define PI 3.14159265358979323846
#define EARTHRADIUS 6371000.f

#define deg2rad(x) ((x)*((PI/180.0)))

struct GameOverPacket{
    qint32 points;
    bool team_infected; //false = team healthy
    bool time_out; //game over because timeout

    void Read(helix::bytestream &stream,bool read_magic = false){
        if(read_magic){
            qint32 magic; magic = stream.get<qint32>();
			if(magic != GAMEOVER_MAGIC){
				qDebug() << "Invalid MAGIC " << magic << " expected " << GAMEOVER_MAGIC;
				return;
			} 
        }
		points = stream.get<qint32>();
		team_infected = stream.get<bool>();
		time_out = stream.get<bool>();
        //stream >> points >> b_infected >> b_time_out;
    }

	void Write(helix::bytestream & stream){
		stream.put<qint32>(GAMEOVER_MAGIC);
		stream.put<qint32>(points);
		stream.put<bool>(team_infected);
		stream.put<bool>(time_out);
	}

    helix::bytes Write(){
		helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream);
        //stream << (qint32) GAMEOVER_MAGIC;
        //stream << points << team_infected << time_out;
		return ret;
    }
};

struct MiniGamePacket{
    qint64 fbid;
    qint32 points;

    void Read(helix::bytestream &stream,bool read_magic = false){
        if(read_magic){
            qint32 magic; magic = stream.get<qint32>();
			if(magic != MINIGAME_MAGIC){
				qDebug() << "Invalid MAGIC " << magic << " expected " << MINIGAME_MAGIC;
				return;
			} 
        }
		fbid = stream.get<qint64>();
		points = stream.get<qint32>();
        //stream >> fbid >> points;
    }

	void Write(helix::bytestream &stream){
		//stream << (qint32)MINIGAME_MAGIC;
		//stream << fbid << points;
		stream.put<qint32>(MINIGAME_MAGIC);        
		stream.put<qint64>(fbid);
		stream.put<qint32>(points); 
	}

    helix::bytes Write(){
		helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream);
		return ret;
    }
};

//Packet from GamesServer to MasterServer
struct GameServerPacket{
    std::string name;
    float lat, lon;
    quint16 port;
    QHostAddress ip;
    quint8 namesize;
    time_t last_update;

    void Read(helix::bytestream &stream,bool read_magic = false){
        if(read_magic){
            qint32 magic;magic = stream.get<qint32>();
            if(magic != GAMESERVER_MAGIC) qDebug() << "Invalid MAGIC " << magic << " expected " << GAMESERVER_MAGIC;
            return;
        }
		namesize = stream.get<qint8>();
        //stream >> namesize;
        //name.resize(namesize);
        //stream.readRawData(&name[0],namesize);
		//stream >> lat >> lon >> ip >> port;

		name = stream.get<std::string>(namesize);

		lat = stream.get<float>();
		lon = stream.get<float>();
		bool isIPv6 = stream.get<bool>();
		if(isIPv6 == true){
			ip.setAddress(stream.get<Q_IPV6ADDR>());			
		}else{
			ip.setAddress(stream.get<quint32>());
		}
		port = stream.get<qint16>();        
    }

	void Write(helix::bytestream &stream, bool write_magic){
	    if(write_magic)stream.put<qint32>(GAMESERVER_MAGIC);

		stream.put<qint8>(name.size());
		stream.put<std::string>(name,name.size());
		stream.put<float>(lat);
		stream.put<float>(lon);
		if(ip.protocol() == QAbstractSocket::IPv6Protocol){
			stream.put<bool>(true);
			stream.put<Q_IPV6ADDR>(ip.toIPv6Address());
		}else{
			stream.put<bool>(false);
			stream.put<quint32>(ip.toIPv4Address());
		}

		stream.put<qint16>(port);
	}

    helix::bytes Write(bool write_magic = true){
		helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream,write_magic);
		return ret;
    }
};

//Packet from MasterServer to Client
struct GameServerListPacket{
    qint16 server_count;
    QList<GameServerPacket> servers;

	GameServerListPacket(){}

    GameServerListPacket(QList<GameServerPacket> &server_list){
        servers.append(server_list);
    }

    void Read(helix::bytestream &stream,bool read_magic = false){
        if(read_magic){
            qint32 magic; magic = stream.get<qint32>();
			if(magic != GAMESERVERLIST_MAGIC){
				qDebug() << "Invalid MAGIC " << magic << " expected " << GAMESERVERLIST_MAGIC;
				return;
			} 
        }
		server_count = stream.get<qint16>();
        GameServerPacket server;
        for(int i = 0; i<server_count && stream.good();++i){
            server.Read(stream,false);
            servers.append(server);
        }
    }

	void Write(helix::bytestream &stream){
		stream.put<qint32>(GAMESERVERLIST_MAGIC);
		server_count = (qint16)servers.size();
		stream.put<qint16>(server_count);
		foreach(GameServerPacket server,servers){
			server.Write(stream,false);
		}
	}

    helix::bytes Write(){
		helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream);
		return ret;
    }
};

//Packet from Client to MasterServer/GameServer
struct ClientPacket{
  float lat, lon;
  qint64 fbid;
  qint32 timestamp;
  bool infected;

  void Read(helix::bytestream &stream){
      //stream >> fbid >> timestamp >> lat >> lon >> infected;
	  fbid = stream.get<qint64>();
	  timestamp = stream.get<qint32>();
	  lat = stream.get<float>();
	  lon = stream.get<float>();
	  infected = stream.get<bool>();
  }

  void Write(helix::bytestream &stream,bool write_magic = true){
  		if(write_magic)stream.put<qint32>(CLIENT_MAGIC);
		//stream << fbid << timestamp << lat << lon << infected;
		stream.put<qint64>(fbid);
		stream.put<qint32>(timestamp);
		stream.put<float>(lat);
		stream.put<float>(lon);
		stream.put<bool>(infected);
  }

  helix::bytes Write(bool write_magic = true){
	  	helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream,write_magic);
		return ret;
  }
  //distance in meters
  float Distance(float ilat, float ilon){
     //quirectangular approximation
     //phi = lat , lamba = lon
     float x = (ilon-lon) * cos((lat+ilat)*0.5f);
     float y = (ilat-lat);

     return sqrt(x*x + y*y)* EARTHRADIUS; //6371 km radius
  }

  float randf(float lo, float hi) {
	  float random = ((float) rand()) / (float) RAND_MAX;
	  float diff = hi - lo;
	  float r = random * diff;
	  return lo + r;
  }

  //radius in meters;
  void randomPoint(float &ilat, float &ilon, float radius){
	radius /= (EARTHRADIUS*cos(ilat));

	float r = sqrtf(((float)rand() / (float) RAND_MAX)) * radius;
	float anglex = 2.f * PI * ((float)rand() / (float) RAND_MAX);
	float angley = 2.f * PI * ((float)rand() / (float) RAND_MAX);
	float coslat =  cosf(ilat); coslat = (coslat == 0.0f) ? FLT_EPSILON : coslat;
	ilon += ( r * cosf (anglex)) / coslat;
	ilat += r * cosf (angley); 
  }

  //moves in the direction of the last two points with a deviation angle 15�
  //returns resulting mounts to x2,y2
  //Vn-2 (x1,y1) = start  Vn-1(x2,y2) = end
  //lon = x lat = y
  void randomMove(float x1, float y1, float &x2, float &y2, float min_distance, float max_dist,float phi){
	  float angle = atan2f(x2-x1,y2-y1); //angle between diff-vector and x axis
	  float anglex = randf(angle-phi, angle+phi); // +- 15 degrees
	  float angley =  randf(angle-phi, angle+phi);
	  float dist = randf(min_distance,max_dist) / (EARTHRADIUS*cos(y2)); //radial distance to move (random between min/max dist)
	  float r = sqrtf(((float)rand() / (float) RAND_MAX)) * dist;
	  float coslat = cosf(y2); coslat = (coslat == 0.0f) ? FLT_EPSILON : coslat;
	  x2 += (r * cosf(anglex)) / coslat;
	  y2 += r * cosf(angley); 
  }
};

//packet from GameServer to Client
struct ClientListPacket{
    qint32 current_score;
    qint16 player_count;    
    QList<ClientPacket> players;

	ClientListPacket(){}

    ClientListPacket(QList<ClientPacket> &player_list){
        players.append(player_list);
    }

	void Read(helix::bytestream &stream, bool read_magic = false){
	    if(read_magic){
            qint32 magic; magic = stream.get<qint32>();
			if(magic != CLIENTLIST_MAGIC) {
				qDebug() << "Invalid MAGIC " << magic << " expected " << CLIENTLIST_MAGIC;
				return;
			}
        }
		current_score = stream.get<qint32>();
		player_count = stream.get<qint16>();
        ClientPacket player;
        for(int i = 0; i<player_count && stream.good();++i){
            player.Read(stream);
            players.append(player);
        }
	}

	void Write(helix::bytestream &stream){
		stream.put<qint32>(CLIENTLIST_MAGIC);
		stream.put<qint32>(current_score);
		player_count = (qint16)players.size();
		stream.put<qint16>(player_count);
		foreach(ClientPacket client,players){
			client.Write(stream,false);
		}
	}

	helix::bytes Write(){
		helix::bytes ret;
		helix::bytestream stream(ret);
		Write(stream);
		return ret;
	}
};

#endif // PACKETDEFINITIONS_H
