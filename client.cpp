#include "client.h"
#include "PacketDefinitions.h"
#include "time.h"
#include <iostream>

Client::Client(int _socket, TcpServer *parent,bool _auto_disconnect)
{
	auto_disconnect = _auto_disconnect;
	parent_server = parent;
	is_gameserver = false;
	socket = _socket;
	client = new QTcpSocket(this);
}

void Client::run(){	
	if(client->setSocketDescriptor(socket) == false){
		std::cout << client->errorString().toStdString() << std::endl;
		remove_from_clientlist();
		return;
	}

	connect(client,SIGNAL(disconnected()),this,SLOT(remove_from_clientlist()));
	connect(client,SIGNAL(readyRead()), this, SLOT(receive_data()));
	connect(client,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(display_error(QAbstractSocket::SocketError)));

	client_ip = client->peerAddress();
	client_port = client->peerPort();

	std::cout << "New incomming connection " << client->peerAddress().toString().toStdString() << ":" << client->peerPort() << std::endl;
}

void Client::receive_data(){
	qint32 magic;
	while(client->bytesAvailable() > 0){
		int bytes_to_read = std::min((int)client->bytesAvailable(),MAX_BUFFER_LENGTH);
		client->read(buffer,bytes_to_read);
		helix::bytes data(buffer,bytes_to_read);
		helix::bytestream stream(data);

		magic = stream.get<qint32>();
		//interpret data
		if(magic == GAMESERVER_MAGIC){
			is_gameserver = true;
			gameserver.Read(stream);
			gameserver.port = client_port;
			gameserver.ip = client_ip;
			time(&(gameserver.last_update));
			parent_server->add_server(gameserver.ip.toString(),gameserver);
			std::cout << "GameServer " << gameserver.name << " registerd" << std::endl;
		}else if(magic == CLIENT_MAGIC){
			is_gameserver = false;
			clientdata.Read(stream);
			//get nearby servers
			GameServerListPacket server_list = parent_server->getServerList(clientdata);
			helix::bytes out = server_list.Write();
			client->write((char*)out.data(),out.size());
			std::cout << "Sending ServerList(" << server_list.server_count << ") to " << client->peerAddress().toString().toStdString() << std::endl;
			if(auto_disconnect){
				//client->flush();
				client->waitForBytesWritten();
			}
		}else{
			std::cout << "Unknown package " << magic << std::endl;
		}
	}
	//not enough data read, somthing is wrong, just for debugging
	if(client->bytesAvailable() > 0)  std::cout << "BytesAvailable " << client->bytesAvailable() << std::endl;

	if(auto_disconnect) remove_from_clientlist();//close the connection once the serverlist was deployed
}

void Client::display_error(QAbstractSocket::SocketError error){
	if(error != 1)std::cout << "SocketError " << error << ": " << client->errorString().toStdString() << std::endl;
}

void Client::remove_from_clientlist(){
	disconnect(client,SIGNAL(disconnected()),this,SLOT(remove_from_clientlist()));
	std::cout << client_ip.toString().toStdString() << " disconnected"<< std::endl;
	client->disconnectFromHost();
	client->close();
	client->deleteLater();
	emit disconnected(this);
}
