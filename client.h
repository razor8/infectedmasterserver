#ifndef CLIENT_H
#define CLIENT_H

#include <QThread>
#include <QTcpSocket>
#include <QHostAddress>
#include "PacketDefinitions.h"
#include "tcpserver.h"

#define MAX_BUFFER_LENGTH 1024

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(int socket,TcpServer *parent,bool auto_disconnect = true);
    bool isGameServer(){return is_gameserver;}
    GameServerPacket getGameServerData(){return gameserver;}
private:
    QTcpSocket* client;
    TcpServer *parent_server;
    int socket;
    GameServerPacket gameserver;
    ClientPacket clientdata;
	bool is_gameserver;
	bool auto_disconnect;
	QHostAddress client_ip;
	quint16 client_port;
	char buffer[MAX_BUFFER_LENGTH];
signals:
    void disconnected(Client *);
private slots:
    void remove_from_clientlist();
	void receive_data();
    void display_error(QAbstractSocket::SocketError error);
public slots:
    void run();
};

#endif // CLIENT_H
