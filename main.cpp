#include <QCoreApplication>
#include "tcpserver.h"
#include <iostream>
#include <signal.h>
#include "Debug.h"

void cleanup(int sig){
	if(sig == SIGINT /*|| sig == SIGTERM*/)qApp->quit();
}

int main(int argc, char *argv[])
{
	HInitDebug();
	QCoreApplication a(argc, argv);
	bool exit = false;
    if(argc > 1){
		TcpServer *server = new TcpServer(64,25.f,600000,&a);//max 64 connections, 25km radius query, 10min update ( 600000 msec)
		QObject::connect(&a,SIGNAL(aboutToQuit()),server,SLOT(free()));
		signal(SIGINT,cleanup);
        if(server->listen((argc > 2) ? QHostAddress(QString(argv[1])) : QHostAddress::Any,(quint16)(atoi(argv[argc > 2 ? 2 : 1])))){
            std::cout << "InfectedMasterServer listening on " << server->serverAddress().toString().toStdString() << ":" << server->serverPort() << std::endl;
		}else{
			std::cout << server->errorString().toStdString() << std::endl;
		}
	}else{
		std::cout << "Invalid number of arguments" << std::endl;
		exit = true;
	}

	if(exit){
		return EXIT_FAILURE;
	}else{
		return a.exec();
	}
}
