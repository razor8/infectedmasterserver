#include "tcpserver.h"
#include "client.h"
#include <iostream>
#include <QCoreApplication>

#define WRITELOCK(x) lock.lockForWrite(); (x); lock.unlock()
#define READLOCK(x) lock.lockForRead(); (x); lock.unlock()

TcpServer::TcpServer(int _max_connections, float _game_radius, int _update_interval, QObject *parent,bool _auto_disconnect): QTcpServer(parent)
{
	auto_disconnect = _auto_disconnect;
    update_interval = _update_interval;
    max_connections = _max_connections;
    game_radius = _game_radius;
    this->setMaxPendingConnections(10);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update_servers()));
	//for finding memleaks
	//QTimer::singleShot(20000,qApp,SLOT(quit()));
    timer->start(_update_interval);
}

void TcpServer::free(){
    delete this;
}

TcpServer::~TcpServer(){
	this->close();
    std::cout << "Stopping InfectedMasterServer" << std::endl;
	//free client objects and threads
	for(QMap<Client*,QThread*>::iterator itr = thread_pool.begin(); itr != thread_pool.end();++itr){
		itr.key()->deleteLater();
        itr.value()->quit();
        itr.value()->wait();
		delete itr.value();
	}    
    timer->stop();
    delete timer;
}

void TcpServer::incomingConnection(int socketDescriptor){
	if(thread_pool.size() < max_connections){
		QThread *thread = new QThread();
		Client *client = new Client(socketDescriptor,this,auto_disconnect);
		client->moveToThread(thread);
		connect(thread, SIGNAL(started()), client, SLOT(run()));
		thread->start();
		connect(client,SIGNAL(disconnected(Client*)),this,SLOT(remove_client(Client*)));
		WRITELOCK(thread_pool.insert(client,thread));
	}
}

void TcpServer::remove_client(Client *client){
    disconnect(client,SIGNAL(disconnected(Client*)),this,SLOT(remove_client(Client*)));
	lock.lockForWrite();    
	QMap<Client*,QThread*>::iterator itr = thread_pool.find(client);
	if(itr != thread_pool.end()){
        itr.value()->quit();
        itr.value()->wait();
        delete itr.value();
		thread_pool.erase(itr);
	}
	lock.unlock();
	delete client;
}

GameServerListPacket TcpServer::getServerList(ClientPacket &location){
    QList<GameServerPacket> nearby_server_list;
    lock.lockForWrite();
    foreach(GameServerPacket packet, server_list){
        float dist = location.Distance(packet.lat,packet.lon);
        if(dist <= game_radius) nearby_server_list.append(packet);
    }
    lock.unlock();
    return GameServerListPacket(nearby_server_list);
}

void TcpServer::add_server(QString IPString, GameServerPacket &packet){
    WRITELOCK(server_list.insert(IPString,packet));
}

void TcpServer::update_servers(){
    time_t end; time(&end);
    //lock.lockForRead();
    lock.lockForWrite();
    for(QMap<QString,GameServerPacket>::iterator itr = server_list.begin();itr != server_list.end();){
        if(difftime(itr->last_update,end) > (double)update_interval){
            std::cout << "Removing " << itr->name << " (timeout)" << std::endl;
            itr = server_list.erase(itr);
        }else{
            itr++;
        }
    }
    lock.unlock();
    std::cout << server_list.size() << " active GameServers" << std::endl;
}
