#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QList>
#include <QMap>
#include <QReadWriteLock>
#include <QTimer>

#include "PacketDefinitions.h"
#include "time.h"

class Client;//forward declaration
class TcpServer: public QTcpServer
{
Q_OBJECT
public:
    TcpServer(int max_connections, float game_radius, int _update_interval, QObject * parent = 0,bool auto_disconnect = true);
    ~TcpServer();
    GameServerListPacket getServerList(ClientPacket &location);
private:
	void incomingConnection(int socketDescriptor);
    QReadWriteLock lock;
    QMap<QString,GameServerPacket> server_list;//map of servers indexed by the server ip-string
	QMap<Client*,QThread*> thread_pool;
    int max_connections;//max number of connctions to this server
    int update_interval;//server update interval in seconds
    float game_radius; //only servers in this radius of the player will be returned
    QTimer *timer;
	bool auto_disconnect;
public slots:
    void free();
    void remove_client(Client *client);
    void add_server(QString IPString,GameServerPacket &packet);
    void update_servers();
};

#endif // TCPSERVER_H
